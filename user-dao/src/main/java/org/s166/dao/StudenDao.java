package org.s166.dao;

import org.s166.entity.Student;


public interface StudenDao {
    /**
     * 查询方法 服务于登录
     * @param id 登录的帐号
     * @param pass  登录的密码
     * @return
     */
    Student login(String id, String pass);

    /**
     * 添加方法 服务于注册
     * @param stu
     * @return
     */
    int addStudent(Student stu);

    /**
     * 查询方法 查看帐号是否存在
     * @param id
     * @return
     */
    Student queryStudent(String id);
}
