package org.s166.dao.impl;


import org.s166.dao.StudenDao;
import org.s166.entity.Student;
import org.s166.user.util.DBUtil;

import java.util.List;
import java.util.Map;

public class StudentDaoImpl implements StudenDao {
    public Student login(String id, String pass) {
        String sql="SELECT * FROM STUDENT WHERE ID=? AND PASS=?";
        Student stu=null;
        List<Map<String, Object>> list = DBUtil.executeQuery(sql, id, pass);
        for (Map<String, Object> s : list) {
            stu=new Student();
            stu.setId(s.get("id").toString());
            stu.setName(s.get("name").toString());
            stu.setAge(Integer.parseInt(s.get("age").toString()));
            stu.setSex(s.get("sex").toString());
        }
        return stu;
    }

    public int addStudent(Student stu) {
        int r=0;
        String sql="insert into student(`id`,`pass`,`name`,`age`,`sex`)VALUES\n" +
                "(?,?,?,?,?)";
        r=DBUtil.executeUpdate(sql,stu.getId(),stu.getPass(),stu.getName(),stu.getAge(),stu.getSex());
        return r;
    }

    @Override
    public Student queryStudent(String id) {
        String sql="SELECT * FROM STUDENT WHERE ID=?";
        Student stu=null;
        List<Map<String, Object>> list = DBUtil.executeQuery(sql, id);
        for (Map<String, Object> s : list) {
            stu=new Student();
            stu.setId(s.get("id").toString());
            stu.setName(s.get("name").toString());
            stu.setAge(Integer.parseInt(s.get("age").toString()));
            stu.setSex(s.get("sex").toString());
        }
        return stu;
    }
}
