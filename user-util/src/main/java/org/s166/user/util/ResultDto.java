package org.s166.user.util;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data   // set get 生成
@AllArgsConstructor //全参构造
@NoArgsConstructor  //默认构造
public class ResultDto {
    //响应的代码
    private int code=200;
    //响应的消息
    private String msg="";
    // 响应的值 一般是list结合 或者对象
    private Object value;
}
