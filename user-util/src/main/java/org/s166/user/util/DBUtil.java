package org.s166.user.util;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class DBUtil {
    // 第一：定义连接数据库的相关信息
    private static final String USER = "root"; 	    // 帐号
    private static final String PASS = "root"; 		// 密码

    // 语法：jdbc:数据库类型://服务器地址:端口号;DatabaseName=数据库名
    // 其中，服务器地址：还可以是IP地址 或 域名 ; localhost表示本地服务器
    private static final String URL = "jdbc:mysql://localhost:3306/test01?useUnicode=true&characterEncoding=utf8&useSSL=false"; // 连接地址（服务器+数据库）

    // 驱动程序 : JDBC接口的实现 ，由数据库厂商提供
    private static final String DRIVER = "com.mysql.jdbc.Driver";


    static {
        try {
            Class.forName(DRIVER);
            //DriverManager.registerDriver(new com.mysql.jdbc.Driver());
        } catch (ClassNotFoundException e) {
            System.out.println("加载驱动失败") ;
        }
    }

    /**
     * 获取连接对象
     * @return 返回连接对象实例
     */
    public static Connection getConnection(){
        Connection conn=null;
        try {
            conn = DriverManager.getConnection(URL,USER,PASS);
        } catch (SQLException throwables) {
            System.out.println("获取连接对象失败") ;
        }
        return conn;
    }

    /**
     * 释放资源方法
     * @param conn 连接对象
     * @param stat 语句对象
     * @param rst   结果集对象
     */
    public static void close(Connection conn, Statement stat, ResultSet rst){
        if(rst!=null){
            try {
                rst.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
                System.out.println("关闭结果集对象失败") ;
            }
        }
        if(stat!=null){
            try {
                stat.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
                System.out.println("关闭语句对象失败") ;
            }
        }
        if(conn!=null){
            try {
                conn.close();
            } catch (SQLException throwables) {
                throwables.printStackTrace();
                System.out.println("关闭连接对象失败") ;
            }
        }
    }

    /**
     * 增删改方法
     * @param sql 增删改sql语句
     * @param params    不定长参数用于填充sql语句的占位符
     * @return  返回受影响的行数
     */
    public static int executeUpdate(String sql,Object...params){
        Connection conn=null;
        PreparedStatement patmt=null;
        conn=getConnection();
        int r=0;
        try {
            patmt=conn.prepareStatement(sql);
            if(params!=null){
                for(int i=0;i<params.length;i++){
                    patmt.setObject(i+1,params[i]);
                }
            }
            r=patmt.executeUpdate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }finally {
            close(conn,patmt,null);
        }
        return r;
    }

    /**
     * 查询方法
     * @param sql 查询sql 语句
     * @param params 填充sql占位符
     * @return  返回查询出来的结果
     */
    public static List<Map<String,Object>> executeQuery(String sql,Object...params){
        List<Map<String,Object>> list=new ArrayList<Map<String,Object>>();
        Connection conn=getConnection();
        PreparedStatement patmt=null;
        ResultSet rst=null;
        try {
            patmt=conn.prepareStatement(sql);
            if(params!=null){
                for(int i=0;i<params.length;i++){
                    patmt.setObject(i+1,params[i]);
                }
            }
            rst = patmt.executeQuery();
            //
            ResultSetMetaData metaData = rst.getMetaData();
            int lieShu = metaData.getColumnCount();
            while (rst.next()){
                Map<String,Object>map=new HashMap<String,Object>();
                for (int i=0;i<lieShu;i++){
                    map.put(metaData.getColumnLabel(i+1),rst.getObject(i+1));
                }
                list.add(map);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }finally {
            close(conn,patmt,rst);
        }
        return list;
    }

}