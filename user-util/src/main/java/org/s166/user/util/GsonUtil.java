package org.s166.user.util;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.util.Map;

public class GsonUtil {
    /**
     * 把Json字符串，转换为Map集合
     *
     * @param jsonData  JSON字符串
     * @return
     */
    public static Map<String, String> parseMap(String jsonData){
        // 创建Gson对象
        Gson gson = new Gson();

        // 转换
        Map<String, String> map = gson.fromJson(jsonData, new TypeToken<Map<String,String>>() {}.getType());

        // 返回结果
        return map;
    }
}
