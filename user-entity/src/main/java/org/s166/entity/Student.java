package org.s166.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data   //set get 生成
@NoArgsConstructor  //默认构造
@AllArgsConstructor //全参构造
public class Student {
    private String id;
    private String pass;
    private String name;
    private int age;
    private String sex;
}
