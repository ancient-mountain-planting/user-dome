package org.s166.userweb;

import com.google.gson.Gson;

import org.s166.dao.StudenDao;
import org.s166.dao.impl.StudentDaoImpl;
import org.s166.entity.Student;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
@WebServlet("/queryStudent.do")
public class QueryIdServlet extends BaseServlet{
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id = req.getParameter("id");
        StudenDao dao=new StudentDaoImpl();

        Student stu = dao.queryStudent(id);
        Gson gson=new Gson();
        if(stu!=null){
            resp.getWriter().print(gson.toJson(successJson(1)));
        }else{
            resp.getWriter().print(gson.toJson(successJson(0)));
        }
        resp.getWriter().flush();
        resp.getWriter().close();
    }
}
