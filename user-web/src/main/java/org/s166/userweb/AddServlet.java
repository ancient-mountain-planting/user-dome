package org.s166.userweb;

import com.google.gson.Gson;
import org.s166.dao.StudenDao;
import org.s166.dao.impl.StudentDaoImpl;
import org.s166.entity.Student;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
@WebServlet("/addUser.do")
public class AddServlet extends BaseServlet{
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id = req.getParameter("id");
        System.out.println(id);
        String pass = req.getParameter("pass");
        String name = req.getParameter("name");
        int age = Integer.parseInt(req.getParameter("age"));
        String sex = req.getParameter("sex");
        Student stu=new Student(id,pass,name,age,sex);
        StudenDao dao=new StudentDaoImpl();
        Gson gson =new Gson();
        int r=dao.addStudent(stu);
        if(r==1){
            resp.getWriter().print(gson.toJson(successJson(null,200,"添加成功")));
        }else{
            resp.getWriter().print(gson.toJson(errorJson("添加失败")));
        }
        resp.getWriter().flush();
        resp.getWriter().close();


    }
}
