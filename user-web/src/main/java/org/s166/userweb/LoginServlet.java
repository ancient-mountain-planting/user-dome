package org.s166.userweb;

import com.google.gson.Gson;
import org.s166.dao.StudenDao;
import org.s166.dao.impl.StudentDaoImpl;
import org.s166.entity.Student;


import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
@WebServlet("/login.do")
public class LoginServlet extends BaseServlet{
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String id = req.getParameter("id");
        String pass = req.getParameter("pass");
        StudenDao dao=new StudentDaoImpl();
        Student stu = dao.login(id, pass);
        Gson gson=new Gson();
        if(stu!=null){
            resp.getWriter().print(gson.toJson(successJson(stu,200,"登陆成功!")));

        }else{
            resp.getWriter().print(gson.toJson(errorJson("帐号或密码错误!")));

        }
        resp.getWriter().flush();
        resp.getWriter().close();
    }
}
