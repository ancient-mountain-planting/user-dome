package org.s166.service.impl;

import org.s166.dao.StudenDao;
import org.s166.dao.impl.StudentDaoImpl;
import org.s166.entity.Student;

import org.s166.service.UserService;

public class UserServiceImpl implements UserService{
    @Override
    public Student login(String id, String pass) {
        StudenDao dao=new StudentDaoImpl();
        Student stu = dao.login(id, pass);
        return stu;
    }

    @Override
    public int addUser(Student stu) {
       int r=0;
        StudenDao dao=new StudentDaoImpl();
        r = dao.addStudent(stu);
        return r;
    }

    @Override
    public int queryLogin(String id) {
        int r=0;
        StudenDao dao=new StudentDaoImpl();
        Student stu = dao.queryStudent(id);
        if(stu!=null){
            r=1;
        }
        return r;
    }
}
