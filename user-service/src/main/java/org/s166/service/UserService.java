package org.s166.service;

import org.s166.entity.Student;

public interface UserService{
    public Student login(String id,String pass);
    public int addUser(Student stu);
    public int queryLogin(String id);
}
